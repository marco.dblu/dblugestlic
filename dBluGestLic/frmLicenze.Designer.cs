﻿using Telerik.WinControls.UI;

namespace dbluGestLic
{
    partial class FrmLicenze
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLicenze));
            this.radPageView1 = new Telerik.WinControls.UI.RadPageView();
            this.radPageViewPage1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.btnRefresh = new Telerik.WinControls.UI.RadButton();
            this.btnGenera = new Telerik.WinControls.UI.RadButton();
            this.radPanel4 = new Telerik.WinControls.UI.RadPanel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radToggleSwitch1 = new Telerik.WinControls.UI.RadToggleSwitch();
            this.rgvPcModuli = new Telerik.WinControls.UI.RadGridView();
            this.radPanel5 = new Telerik.WinControls.UI.RadPanel();
            this.radGridViewModuli = new Telerik.WinControls.UI.RadGridView();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radPageViewPage2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.radPageView2 = new Telerik.WinControls.UI.RadPageView();
            this.radPageViewPage3 = new Telerik.WinControls.UI.RadPageViewPage();
            this.txtRagSoc = new Telerik.WinControls.UI.RadTextBox();
            this.txtPIVA = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radPageViewPage4 = new Telerik.WinControls.UI.RadPageViewPage();
            this.txtCodiceFiscale = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.txtCognome = new Telerik.WinControls.UI.RadTextBox();
            this.txtNome = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.S = new Telerik.WinControls.UI.RadLabel();
            this.radPageView3 = new Telerik.WinControls.UI.RadPageView();
            this.radPageViewPage5 = new Telerik.WinControls.UI.RadPageViewPage();
            this.labelRef = new Telerik.WinControls.UI.RadLabel();
            this.txtEmailRef = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.labelEmail = new Telerik.WinControls.UI.RadLabel();
            this.txtReferente = new Telerik.WinControls.UI.RadTextBox();
            this.btnSalva = new Telerik.WinControls.UI.RadButton();
            this.mainPanel = new Telerik.WinControls.UI.RadPanel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.btnInfo = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.radSplitContainer2 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel3 = new Telerik.WinControls.UI.SplitPanel();
            this.splitPanel4 = new Telerik.WinControls.UI.SplitPanel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Modulo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Qta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.visualStudio2012LightTheme1 = new Telerik.WinControls.Themes.VisualStudio2012LightTheme();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).BeginInit();
            this.radPageView1.SuspendLayout();
            this.radPageViewPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnRefresh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnGenera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).BeginInit();
            this.radPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgvPcModuli)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgvPcModuli.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel5)).BeginInit();
            this.radPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewModuli)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewModuli.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            this.radPageViewPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView2)).BeginInit();
            this.radPageView2.SuspendLayout();
            this.radPageViewPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRagSoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            this.radLabel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            this.radPageViewPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodiceFiscale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCognome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            this.radLabel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            this.radLabel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.S)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView3)).BeginInit();
            this.radPageView3.SuspendLayout();
            this.radPageViewPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmailRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReferente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSalva)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainPanel)).BeginInit();
            this.mainPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).BeginInit();
            this.radSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).BeginInit();
            this.splitPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radPageView1
            // 
            this.radPageView1.Controls.Add(this.radPageViewPage1);
            this.radPageView1.Controls.Add(this.radPageViewPage2);
            this.radPageView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPageView1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radPageView1.Location = new System.Drawing.Point(0, 68);
            this.radPageView1.Margin = new System.Windows.Forms.Padding(0);
            this.radPageView1.Name = "radPageView1";
            this.radPageView1.SelectedPage = this.radPageViewPage1;
            this.radPageView1.Size = new System.Drawing.Size(1353, 618);
            this.radPageView1.TabIndex = 6;
            this.radPageView1.ThemeName = "VisualStudio2012Light";
            // 
            // radPageViewPage1
            // 
            this.radPageViewPage1.Controls.Add(this.btnRefresh);
            this.radPageViewPage1.Controls.Add(this.btnGenera);
            this.radPageViewPage1.Controls.Add(this.radPanel4);
            this.radPageViewPage1.Controls.Add(this.radPanel5);
            this.radPageViewPage1.ItemSize = new System.Drawing.SizeF(90F, 31F);
            this.radPageViewPage1.Location = new System.Drawing.Point(5, 37);
            this.radPageViewPage1.Name = "radPageViewPage1";
            this.radPageViewPage1.Size = new System.Drawing.Size(1343, 576);
            this.radPageViewPage1.Text = "Attivazioni";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnRefresh.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.btnRefresh.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefresh.Image = global::dbluGestLic.Properties.Resources.baseline_refresh_black_24dp;
            this.btnRefresh.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnRefresh.Location = new System.Drawing.Point(1114, 515);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(50, 50);
            this.btnRefresh.TabIndex = 15;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.ThemeName = "VisualStudio2012Light";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnGenera
            // 
            this.btnGenera.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGenera.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenera.Location = new System.Drawing.Point(1170, 515);
            this.btnGenera.Name = "btnGenera";
            this.btnGenera.Size = new System.Drawing.Size(163, 50);
            this.btnGenera.TabIndex = 14;
            this.btnGenera.Text = "Genera Licenza";
            this.btnGenera.ThemeName = "VisualStudio2012Light";
            this.btnGenera.Click += new System.EventHandler(this.BtnGenera_Click);
            // 
            // radPanel4
            // 
            this.radPanel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.radPanel4.Controls.Add(this.radLabel5);
            this.radPanel4.Controls.Add(this.radToggleSwitch1);
            this.radPanel4.Controls.Add(this.rgvPcModuli);
            this.radPanel4.Location = new System.Drawing.Point(11, 3);
            this.radPanel4.Name = "radPanel4";
            this.radPanel4.Size = new System.Drawing.Size(700, 505);
            this.radPanel4.TabIndex = 12;
            this.radPanel4.ThemeName = "VisualStudio2012Light";
            // 
            // radLabel5
            // 
            this.radLabel5.AutoSize = false;
            this.radLabel5.BackColor = System.Drawing.Color.Transparent;
            this.radLabel5.BackgroundImage = global::dbluGestLic.Properties.Resources.computer_outline_png_7;
            this.radLabel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.radLabel5.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radLabel5.Location = new System.Drawing.Point(3, 4);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(61, 58);
            this.radLabel5.TabIndex = 9;
            // 
            // radToggleSwitch1
            // 
            this.radToggleSwitch1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radToggleSwitch1.Location = new System.Drawing.Point(526, 4);
            this.radToggleSwitch1.Name = "radToggleSwitch1";
            this.radToggleSwitch1.OffText = "Moduli";
            this.radToggleSwitch1.OnText = "Stazione";
            this.radToggleSwitch1.Size = new System.Drawing.Size(171, 57);
            this.radToggleSwitch1.TabIndex = 7;
            this.radToggleSwitch1.ThemeName = "VisualStudio2012Light";
            this.radToggleSwitch1.ThumbTickness = 15;
            this.radToggleSwitch1.ValueChanged += new System.EventHandler(this.RadToggleSwitch1_ValueChanged);
            // 
            // rgvPcModuli
            // 
            this.rgvPcModuli.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.rgvPcModuli.BeginEditMode = Telerik.WinControls.RadGridViewBeginEditMode.BeginEditOnEnter;
            this.rgvPcModuli.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rgvPcModuli.Location = new System.Drawing.Point(3, 67);
            // 
            // 
            // 
            this.rgvPcModuli.MasterTemplate.AllowAddNewRow = false;
            this.rgvPcModuli.MasterTemplate.ShowRowHeaderColumn = false;
            this.rgvPcModuli.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.rgvPcModuli.Name = "rgvPcModuli";
            this.rgvPcModuli.ShowGroupPanel = false;
            this.rgvPcModuli.Size = new System.Drawing.Size(694, 423);
            this.rgvPcModuli.TabIndex = 6;
            this.rgvPcModuli.ThemeName = "VisualStudio2012Light";
            this.rgvPcModuli.ValueChanged += new System.EventHandler(this.RgvPcModuli_ValueChanged);
            this.rgvPcModuli.ValueChanging += new Telerik.WinControls.UI.ValueChangingEventHandler(this.RgvPcModuli_ValueChanging);
            this.rgvPcModuli.SelectionChanging += new Telerik.WinControls.UI.GridViewSelectionCancelEventHandler(this.RgvPcModuli_SelectionChanging);
            this.rgvPcModuli.SelectionChanged += new System.EventHandler(this.RgvPcModuli_SelectionChanged);
            this.rgvPcModuli.Click += new System.EventHandler(this.RgvPcModuli_Click);
            // 
            // radPanel5
            // 
            this.radPanel5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radPanel5.Controls.Add(this.radGridViewModuli);
            this.radPanel5.Controls.Add(this.radLabel7);
            this.radPanel5.Location = new System.Drawing.Point(723, 3);
            this.radPanel5.Name = "radPanel5";
            this.radPanel5.Size = new System.Drawing.Size(610, 505);
            this.radPanel5.TabIndex = 13;
            this.radPanel5.ThemeName = "MedicalAppTheme";
            // 
            // radGridViewModuli
            // 
            this.radGridViewModuli.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.radGridViewModuli.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGridViewModuli.Location = new System.Drawing.Point(3, 67);
            // 
            // 
            // 
            this.radGridViewModuli.MasterTemplate.AllowAddNewRow = false;
            this.radGridViewModuli.MasterTemplate.ShowRowHeaderColumn = false;
            this.radGridViewModuli.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.radGridViewModuli.Name = "radGridViewModuli";
            this.radGridViewModuli.Size = new System.Drawing.Size(594, 423);
            this.radGridViewModuli.TabIndex = 7;
            this.radGridViewModuli.ThemeName = "VisualStudio2012Light";
            // 
            // radLabel7
            // 
            this.radLabel7.AutoSize = false;
            this.radLabel7.BackColor = System.Drawing.Color.Transparent;
            this.radLabel7.BackgroundImage = global::dbluGestLic.Properties.Resources.licenza1;
            this.radLabel7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.radLabel7.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radLabel7.Location = new System.Drawing.Point(3, 3);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(61, 58);
            this.radLabel7.TabIndex = 2;
            // 
            // radPageViewPage2
            // 
            this.radPageViewPage2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.radPageViewPage2.Controls.Add(this.radButton1);
            this.radPageViewPage2.Controls.Add(this.radPageView2);
            this.radPageViewPage2.Controls.Add(this.radPageView3);
            this.radPageViewPage2.Controls.Add(this.btnSalva);
            this.radPageViewPage2.ItemSize = new System.Drawing.SizeF(72F, 31F);
            this.radPageViewPage2.Location = new System.Drawing.Point(5, 37);
            this.radPageViewPage2.Name = "radPageViewPage2";
            this.radPageViewPage2.Size = new System.Drawing.Size(1343, 567);
            this.radPageViewPage2.Text = "Azienda";
            // 
            // radButton1
            // 
            this.radButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton1.Location = new System.Drawing.Point(11, 510);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(163, 28);
            this.radButton1.TabIndex = 1;
            this.radButton1.Text = "Cambia Password";
            this.radButton1.ThemeName = "VisualStudio2012Light";
            this.radButton1.Click += new System.EventHandler(this.RadButton1_Click);
            // 
            // radPageView2
            // 
            this.radPageView2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radPageView2.Controls.Add(this.radPageViewPage3);
            this.radPageView2.Controls.Add(this.radPageViewPage4);
            this.radPageView2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radPageView2.Location = new System.Drawing.Point(3, 3);
            this.radPageView2.Name = "radPageView2";
            this.radPageView2.SelectedPage = this.radPageViewPage3;
            this.radPageView2.Size = new System.Drawing.Size(1320, 160);
            this.radPageView2.TabIndex = 25;
            this.radPageView2.ThemeName = "VisualStudio2012Light";
            // 
            // radPageViewPage3
            // 
            this.radPageViewPage3.Controls.Add(this.txtRagSoc);
            this.radPageViewPage3.Controls.Add(this.txtPIVA);
            this.radPageViewPage3.Controls.Add(this.radLabel6);
            this.radPageViewPage3.Controls.Add(this.radLabel8);
            this.radPageViewPage3.Location = new System.Drawing.Point(10, 44);
            this.radPageViewPage3.Name = "radPageViewPage3";
            this.radPageViewPage3.Size = new System.Drawing.Size(1299, 105);
            this.radPageViewPage3.Text = "Giuridica";
            // 
            // txtRagSoc
            // 
            this.txtRagSoc.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRagSoc.Location = new System.Drawing.Point(3, 29);
            this.txtRagSoc.Name = "txtRagSoc";
            this.txtRagSoc.Size = new System.Drawing.Size(384, 27);
            this.txtRagSoc.TabIndex = 13;
            this.txtRagSoc.ThemeName = "VisualStudio2012Light";
            // 
            // txtPIVA
            // 
            this.txtPIVA.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPIVA.Location = new System.Drawing.Point(3, 88);
            this.txtPIVA.Name = "txtPIVA";
            this.txtPIVA.Size = new System.Drawing.Size(384, 27);
            this.txtPIVA.TabIndex = 1;
            this.txtPIVA.TabStop = false;
            this.txtPIVA.ThemeName = "VisualStudio2012Light";
            // 
            // radLabel6
            // 
            this.radLabel6.Controls.Add(this.radLabel3);
            this.radLabel6.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel6.Location = new System.Drawing.Point(3, 62);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(84, 25);
            this.radLabel6.TabIndex = 12;
            this.radLabel6.Text = "Partita IVA";
            this.radLabel6.ThemeName = "VisualStudio2012Light";
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.Location = new System.Drawing.Point(-188, -15);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(84, 25);
            this.radLabel3.TabIndex = 13;
            this.radLabel3.Text = "Partita IVA";
            this.radLabel3.ThemeName = "VisualStudio2012Light";
            // 
            // radLabel8
            // 
            this.radLabel8.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel8.Location = new System.Drawing.Point(0, 3);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(123, 25);
            this.radLabel8.TabIndex = 11;
            this.radLabel8.Text = "Ragione Sociale";
            this.radLabel8.ThemeName = "VisualStudio2012Light";
            // 
            // radPageViewPage4
            // 
            this.radPageViewPage4.Controls.Add(this.txtCodiceFiscale);
            this.radPageViewPage4.Controls.Add(this.txtCognome);
            this.radPageViewPage4.Controls.Add(this.txtNome);
            this.radPageViewPage4.Controls.Add(this.radLabel13);
            this.radPageViewPage4.Controls.Add(this.radLabel9);
            this.radPageViewPage4.Controls.Add(this.S);
            this.radPageViewPage4.Enabled = false;
            this.radPageViewPage4.Location = new System.Drawing.Point(5, 37);
            this.radPageViewPage4.Name = "radPageViewPage4";
            this.radPageViewPage4.Size = new System.Drawing.Size(1310, 118);
            this.radPageViewPage4.Text = "Persona Fisica";
            // 
            // txtCodiceFiscale
            // 
            this.txtCodiceFiscale.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodiceFiscale.Location = new System.Drawing.Point(3, 88);
            this.txtCodiceFiscale.Mask = "###########";
            this.txtCodiceFiscale.MaskType = Telerik.WinControls.UI.MaskType.Standard;
            this.txtCodiceFiscale.Name = "txtCodiceFiscale";
            this.txtCodiceFiscale.Size = new System.Drawing.Size(384, 27);
            this.txtCodiceFiscale.TabIndex = 2;
            this.txtCodiceFiscale.TabStop = false;
            this.txtCodiceFiscale.Text = "___________";
            this.txtCodiceFiscale.ThemeName = "VisualStudio2012Light";
            // 
            // txtCognome
            // 
            this.txtCognome.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCognome.Location = new System.Drawing.Point(3, 29);
            this.txtCognome.Name = "txtCognome";
            this.txtCognome.Size = new System.Drawing.Size(384, 27);
            this.txtCognome.TabIndex = 0;
            this.txtCognome.ThemeName = "VisualStudio2012Light";
            // 
            // txtNome
            // 
            this.txtNome.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNome.Location = new System.Drawing.Point(393, 29);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(258, 27);
            this.txtNome.TabIndex = 1;
            this.txtNome.ThemeName = "VisualStudio2012Light";
            // 
            // radLabel13
            // 
            this.radLabel13.Controls.Add(this.radLabel14);
            this.radLabel13.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel13.Location = new System.Drawing.Point(3, 62);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(110, 25);
            this.radLabel13.TabIndex = 16;
            this.radLabel13.Text = "Codice Fiscale";
            this.radLabel13.ThemeName = "VisualStudio2012Light";
            // 
            // radLabel14
            // 
            this.radLabel14.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel14.Location = new System.Drawing.Point(-188, -15);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(84, 25);
            this.radLabel14.TabIndex = 13;
            this.radLabel14.Text = "Partita IVA";
            this.radLabel14.ThemeName = "VisualStudio2012Light";
            // 
            // radLabel9
            // 
            this.radLabel9.Controls.Add(this.radLabel10);
            this.radLabel9.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel9.Location = new System.Drawing.Point(393, 3);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(53, 25);
            this.radLabel9.TabIndex = 15;
            this.radLabel9.Text = "Nome";
            this.radLabel9.ThemeName = "VisualStudio2012Light";
            // 
            // radLabel10
            // 
            this.radLabel10.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel10.Location = new System.Drawing.Point(-188, -15);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(84, 25);
            this.radLabel10.TabIndex = 13;
            this.radLabel10.Text = "Partita IVA";
            this.radLabel10.ThemeName = "VisualStudio2012Light";
            // 
            // S
            // 
            this.S.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.S.Location = new System.Drawing.Point(3, 3);
            this.S.Name = "S";
            this.S.Size = new System.Drawing.Size(79, 25);
            this.S.TabIndex = 14;
            this.S.Text = "Cognome";
            this.S.ThemeName = "VisualStudio2012Light";
            // 
            // radPageView3
            // 
            this.radPageView3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radPageView3.Controls.Add(this.radPageViewPage5);
            this.radPageView3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radPageView3.Location = new System.Drawing.Point(3, 169);
            this.radPageView3.Name = "radPageView3";
            this.radPageView3.SelectedPage = this.radPageViewPage5;
            this.radPageView3.Size = new System.Drawing.Size(1320, 162);
            this.radPageView3.TabIndex = 26;
            this.radPageView3.ThemeName = "VisualStudio2012Light";
            // 
            // radPageViewPage5
            // 
            this.radPageViewPage5.Controls.Add(this.labelRef);
            this.radPageViewPage5.Controls.Add(this.txtEmailRef);
            this.radPageViewPage5.Controls.Add(this.labelEmail);
            this.radPageViewPage5.Controls.Add(this.txtReferente);
            this.radPageViewPage5.Location = new System.Drawing.Point(10, 44);
            this.radPageViewPage5.Name = "radPageViewPage5";
            this.radPageViewPage5.Size = new System.Drawing.Size(1299, 107);
            this.radPageViewPage5.Text = "Referente";
            // 
            // labelRef
            // 
            this.labelRef.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRef.Location = new System.Drawing.Point(3, 3);
            this.labelRef.Name = "labelRef";
            this.labelRef.Size = new System.Drawing.Size(127, 25);
            this.labelRef.TabIndex = 18;
            this.labelRef.Text = "Nome Referente";
            this.labelRef.ThemeName = "VisualStudio2012Light";
            // 
            // txtEmailRef
            // 
            this.txtEmailRef.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmailRef.Location = new System.Drawing.Point(3, 90);
            this.txtEmailRef.Name = "txtEmailRef";
            this.txtEmailRef.Size = new System.Drawing.Size(384, 27);
            this.txtEmailRef.TabIndex = 1;
            this.txtEmailRef.TabStop = false;
            this.txtEmailRef.ThemeName = "VisualStudio2012Light";
            // 
            // labelEmail
            // 
            this.labelEmail.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEmail.Location = new System.Drawing.Point(3, 59);
            this.labelEmail.Name = "labelEmail";
            this.labelEmail.Size = new System.Drawing.Size(121, 25);
            this.labelEmail.TabIndex = 17;
            this.labelEmail.Text = "Email Referente";
            this.labelEmail.ThemeName = "VisualStudio2012Light";
            // 
            // txtReferente
            // 
            this.txtReferente.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReferente.Location = new System.Drawing.Point(3, 28);
            this.txtReferente.Name = "txtReferente";
            this.txtReferente.Size = new System.Drawing.Size(384, 27);
            this.txtReferente.TabIndex = 0;
            this.txtReferente.ThemeName = "VisualStudio2012Light";
            // 
            // btnSalva
            // 
            this.btnSalva.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSalva.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalva.Location = new System.Drawing.Point(1170, 510);
            this.btnSalva.Name = "btnSalva";
            this.btnSalva.Size = new System.Drawing.Size(163, 28);
            this.btnSalva.TabIndex = 0;
            this.btnSalva.Text = "Salva";
            this.btnSalva.ThemeName = "VisualStudio2012Light";
            this.btnSalva.Click += new System.EventHandler(this.BtnSalva_Click);
            // 
            // mainPanel
            // 
            this.mainPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(24)))), ((int)(((byte)(58)))));
            this.mainPanel.Controls.Add(this.radLabel4);
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.mainPanel.Location = new System.Drawing.Point(0, 0);
            this.mainPanel.Margin = new System.Windows.Forms.Padding(0);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(1353, 68);
            this.mainPanel.TabIndex = 4;
            // 
            // radLabel4
            // 
            this.radLabel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel4.AutoSize = false;
            this.radLabel4.BackColor = System.Drawing.Color.Transparent;
            this.radLabel4.BackgroundImage = global::dbluGestLic.Properties.Resources.dBluBi;
            this.radLabel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.radLabel4.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radLabel4.Location = new System.Drawing.Point(1206, 5);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(135, 50);
            this.radLabel4.TabIndex = 3;
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.EnableGestures = false;
            this.radSplitContainer1.Location = new System.Drawing.Point(720, 348);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(138, 86);
            this.radSplitContainer1.TabIndex = 5;
            this.radSplitContainer1.TabStop = false;
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.btnInfo);
            this.splitPanel1.Controls.Add(this.label1);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(138, 7);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.4147982F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -185);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // btnInfo
            // 
            this.btnInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInfo.Location = new System.Drawing.Point(15, 3);
            this.btnInfo.Name = "btnInfo";
            this.btnInfo.Size = new System.Drawing.Size(111, 1);
            this.btnInfo.TabIndex = 1;
            this.btnInfo.Text = "Info";
            this.btnInfo.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "lblAzienda";
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.radSplitContainer2);
            this.splitPanel2.Location = new System.Drawing.Point(0, 11);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(138, 75);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.4147982F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 185);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // radSplitContainer2
            // 
            this.radSplitContainer2.Controls.Add(this.splitPanel3);
            this.radSplitContainer2.Controls.Add(this.splitPanel4);
            this.radSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer2.Name = "radSplitContainer2";
            // 
            // 
            // 
            this.radSplitContainer2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer2.Size = new System.Drawing.Size(138, 75);
            this.radSplitContainer2.TabIndex = 0;
            this.radSplitContainer2.TabStop = false;
            // 
            // splitPanel3
            // 
            this.splitPanel3.Location = new System.Drawing.Point(0, 0);
            this.splitPanel3.Name = "splitPanel3";
            // 
            // 
            // 
            this.splitPanel3.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel3.Size = new System.Drawing.Size(99, 75);
            this.splitPanel3.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.241206F, 0F);
            this.splitPanel3.SizeInfo.SplitterCorrection = new System.Drawing.Size(192, 0);
            this.splitPanel3.TabIndex = 0;
            this.splitPanel3.TabStop = false;
            this.splitPanel3.Text = "splitPanel3";
            // 
            // splitPanel4
            // 
            this.splitPanel4.Controls.Add(this.dataGridView1);
            this.splitPanel4.Location = new System.Drawing.Point(103, 0);
            this.splitPanel4.Name = "splitPanel4";
            // 
            // 
            // 
            this.splitPanel4.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel4.Size = new System.Drawing.Size(35, 75);
            this.splitPanel4.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(-0.241206F, 0F);
            this.splitPanel4.SizeInfo.SplitterCorrection = new System.Drawing.Size(-192, 0);
            this.splitPanel4.TabIndex = 1;
            this.splitPanel4.TabStop = false;
            this.splitPanel4.Text = "splitPanel4";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Modulo,
            this.Qta});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(35, 75);
            this.dataGridView1.TabIndex = 0;
            // 
            // Modulo
            // 
            this.Modulo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Modulo.HeaderText = "Modulo";
            this.Modulo.Name = "Modulo";
            this.Modulo.ReadOnly = true;
            // 
            // Qta
            // 
            this.Qta.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Qta.HeaderText = "Qta";
            this.Qta.Name = "Qta";
            this.Qta.ReadOnly = true;
            this.Qta.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Qta.Width = 50;
            // 
            // FrmLicenze
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1353, 677);
            this.Controls.Add(this.radPageView1);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.radSplitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmLicenze";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "dBlu Gestione Licenze";
            this.Text = "dBlu";
            this.ThemeName = "VisualStudio2012Light";
            this.Load += new System.EventHandler(this.RadForm1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).EndInit();
            this.radPageView1.ResumeLayout(false);
            this.radPageViewPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnRefresh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnGenera)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).EndInit();
            this.radPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgvPcModuli.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgvPcModuli)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel5)).EndInit();
            this.radPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewModuli.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewModuli)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            this.radPageViewPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView2)).EndInit();
            this.radPageView2.ResumeLayout(false);
            this.radPageViewPage3.ResumeLayout(false);
            this.radPageViewPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRagSoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            this.radLabel6.ResumeLayout(false);
            this.radLabel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            this.radPageViewPage4.ResumeLayout(false);
            this.radPageViewPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodiceFiscale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCognome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            this.radLabel13.ResumeLayout(false);
            this.radLabel13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            this.radLabel9.ResumeLayout(false);
            this.radLabel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.S)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView3)).EndInit();
            this.radPageView3.ResumeLayout(false);
            this.radPageViewPage5.ResumeLayout(false);
            this.radPageViewPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmailRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReferente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSalva)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainPanel)).EndInit();
            this.mainPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            this.splitPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).EndInit();
            this.radSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).EndInit();
            this.splitPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadPageView radPageView1;
        private Telerik.WinControls.UI.RadPanel mainPanel;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private System.Windows.Forms.Button btnInfo;
        private System.Windows.Forms.Label label1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer2;
        private Telerik.WinControls.UI.SplitPanel splitPanel3;
        private Telerik.WinControls.UI.SplitPanel splitPanel4;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Modulo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Qta;
        private RadPageViewPage radPageViewPage1;
        private RadPanel radPanel4;
        private RadGridView rgvPcModuli;
        private RadPanel radPanel5;
        private RadGridView radGridViewModuli;
        private RadLabel radLabel7;
        private RadToggleSwitch radToggleSwitch1;
        private RadPageViewPage radPageViewPage2;
        private Telerik.WinControls.Themes.VisualStudio2012LightTheme visualStudio2012LightTheme1;
        private RadLabel radLabel4;
        private RadButton btnGenera;
        private RadLabel radLabel5;
        private RadButton btnSalva;
        private RadPageView radPageView2;
        private RadPageViewPage radPageViewPage3;
        private RadMaskedEditBox txtPIVA;
        private RadLabel radLabel6;
        private RadLabel radLabel3;
        private RadLabel radLabel8;
        private RadPageViewPage radPageViewPage4;
        private RadMaskedEditBox txtCodiceFiscale;
        private RadTextBox txtCognome;
        private RadTextBox txtNome;
        private RadLabel radLabel13;
        private RadLabel radLabel14;
        private RadLabel radLabel9;
        private RadLabel radLabel10;
        private RadLabel S;
        private RadPageView radPageView3;
        private RadPageViewPage radPageViewPage5;
        private RadLabel labelRef;
        private RadMaskedEditBox txtEmailRef;
        private RadLabel labelEmail;
        private RadTextBox txtReferente;
        private RadButton radButton1;
        private RadTextBox txtRagSoc;
        private RadButton btnRefresh;
    }
}