﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dbluGestLic.Classi
{
    public static class extension
    {
        public static string Inverti(this string str)
        {
            char[] array = new char[str.Length];
            int forward = 0;
            for (int i = str.Length - 1; i >= 0; i--)
            {
                array[forward++] = str[i];
            }
            return new string(array);
        }
    }
}
