﻿using Newtonsoft.Json;
using NLog;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dblu.Licenze.Models;
using NLog;

namespace dbluGestLic.Classi
{
    public class licClient
    {
        private string Token { get; set; }
        private static readonly Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private string Url { get; set; }
        public string Message { get; set; }

        public licClient() {
            this.Token = "";
            this.Url = Properties.Settings.Default.serviceUrl;
        }

        public bool Login(string User, string Password) {

            try
            {
                this.Message = "";
                var client = new RestClient($"{Url}/api/token")
                {
                    Timeout = -1
                };
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                //request.AddHeader("Content-Type", "text/plain");
                dynamic par = new ExpandoObject();
                par.Username = User;
                par.Password = Password;
                //request.AddParameter("application/json,text/plain", $"{\"UserName\": \"{}\",\n \"Password\" : \"ciLtseGulbd\"\n}", ParameterType.RequestBody);
                request.AddParameter("application/json", JsonConvert.SerializeObject(par), ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                if (response.IsSuccessful)
                {
                    this.Token = JsonConvert.DeserializeObject<string>(response.Content);
                }
                else {
                    this.Message = JsonConvert.DeserializeObject<string>(response.Content);
                }
                return response.IsSuccessful;
            }
            catch ( Exception ex)
            {
                _logger.Error(ex, "Login");
            }
            return false;
        }

        public bool GetAppCliente(string IdKeyCli, string IdKeyLic, out AppCliente app)
        {
            app = null;
            try
            {
                this.Message = "";
                var client = new RestClient($"{Url}/api/lic/GetAppCliente")
                {
                    Timeout = -1
                };
                var request = new RestRequest(Method.GET);
                //request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Authorization", $"Bearer {this.Token}");
                request.AddParameter("IdKeyCli", IdKeyCli);
                request.AddParameter("IdKeyLic", IdKeyLic);
                IRestResponse response = client.Execute(request);
                if (response.IsSuccessful)
                {
                    app = JsonConvert.DeserializeObject<AppCliente>(response.Content);
                }
                else
                {
                    this.Message = JsonConvert.DeserializeObject<string>(response.Content);
                }
                return response.IsSuccessful;
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "GetAppCliente");
            }
            return false;
        }

        public bool RegistraAppCliente(Licenza lic, out Licenza licFirmata)
        {
            licFirmata = null;
            try
            {
                this.Message = "";
                var client = new RestClient($"{Url}/api/lic/RegistraAppCliente")
                {
                    Timeout = -1
                };
                var request = new RestRequest(Method.POST);
                //request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Authorization", $"Bearer {this.Token}");
                request.AddParameter("application/json", JsonConvert.SerializeObject(lic), ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                if (response.IsSuccessful)
                {
                    licFirmata = JsonConvert.DeserializeObject<Licenza>(response.Content);
                }
                else
                {
                    this.Message = JsonConvert.DeserializeObject<string>(response.Content);
                }
                return response.IsSuccessful;
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "RegistraAppCliente");
            }
            return false;
        }


    }
}
