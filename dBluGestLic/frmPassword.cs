﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using dbluGestLic.Classi;

namespace dbluGestLic
{
    public partial class frmPassword : Telerik.WinControls.UI.RadForm
    {
        private string PasswordCrypt { get; set; } = "";

        public frmPassword()
        {
            InitializeComponent();
            Utility.MySettings settings = Utility.MySettings.Load();
            PasswordCrypt = settings.Password;
        }

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            if (VerificaLogin(txtPassword.Text) == true)
            {
                this.DialogResult = DialogResult.OK;
            }
        }

        private bool VerificaLogin(string password)
        {
            bool res = false;
            string hash;  // hash della password salvata
            hash = PasswordCrypt;

            if (String.IsNullOrEmpty(hash))
            {
                using (MD5 md5Hash = MD5.Create())
                {
                    hash = Utility.GetMd5Hash(md5Hash, "dblu");
                }
            }

            using (MD5 md5Hash = MD5.Create())
            {
                if (Utility.VerifyMd5Hash(md5Hash, password, hash))
                {
                    res = true;
                }
                else
                {
                    res = false;
                    MessageBox.Show("La password inserita non è corretta");
                }
            }
            return res;
        }


        private void TxtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (VerificaLogin(txtPassword.Text) == true)
                {
                    this.DialogResult = DialogResult.OK;
                }
            }
        }

        private void BtnResetPassword_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Vuoi resettare la password?", "Reset Password", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                string hash;  // hash della password salvata
                using (MD5 md5Hash = MD5.Create())
                {
                    PasswordCrypt = Utility.GetMd5Hash(md5Hash, "dblu");
                }
                Utility.MySettings settings = Utility.MySettings.Load();
                settings.Password = PasswordCrypt;
                settings.Save();
                
                MessageBox.Show("La password è stata reimpostata", "dblu", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void BtnAnnulla_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}