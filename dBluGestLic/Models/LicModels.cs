﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace dblu.Licenze.Models
{
    public partial class LicApplicativi
    {
        public string IdKeyApp { get; set; }
        public string Gruppo { get; set; }
        public string Nome { get; set; }
        public string Guid { get; set; }
        public string Modulo { get; set; }
        public string IdArtFatt { get; set; }
    }

    public partial class LicCliApp
    {
        public string IdKeyCli { get; set; }
        public string IdKeyApp { get; set; }
        public int? Qta { get; set; }
        public DateTime? DataScad { get; set; }
        public int? QtaResidua { get; set; }
        public string Note { get; set; }
        public int? Attivo { get; set; }
        public DateTime? DataC { get; set; }
        public DateTime? DataUM { get; set; }
    }
    public partial class LicClienti
    {
        public string IdKeyCli { get; set; }
        public string IdCliFatt { get; set; }
        public string RagSoc { get; set; }
        public string Referente { get; set; }
        public string Email { get; set; }
    }


    public partial class LicCliComp
    {
        public string IdKeyCli { get; set; }
        public Guid? IdKeyComp { get; set; }
        public string ComputerName { get; set; }
        public string CheckPc { get; set; }
        public string Note { get; set; }
    }

    public partial class LicKey
    {
        public string IdKeyCli { get; set; }
        public Guid IdKey { get; set; }
        public string Path { get; set; }
        public string Referente2 { get; set; }
        public string Email2 { get; set; }
        public string Note { get; set; }
    }

    public partial class LicKeyComp
    {
        [JsonIgnore]
        public Guid IdKey { get; set; }

        public string IdKeyApp { get; set; }
        [JsonIgnore]
        public Guid? IdKeyComp { get; set; }
        public DateTime? DataScad { get; set; }
        public string Note { get; set; }
    }

    public partial class vLicCliApp
    {

        public string IdKeyCli { get; set; }
        public string IdKeyApp { get; set; }
        public string Gruppo { get; set; }
        public string Nome { get; set; }
        public string Modulo { get; set; }
        public int? Qta { get; set; }
        public DateTime? DataScad { get; set; }
        public int? QtaResidua { get; set; }
        public int? Attivo { get; set; }

    }

    //public class CompCliente
    //{
    //    public LicCliComp Postazione { get; set; }
    //    public List<LicKeyComp> Applicazioni { get; set; }

    //}

    public class AppCliente
    {
        public LicClienti Cliente { get; set; }
        public List<vLicCliApp> Applicazioni { get; set; }
        public List<LicCliComp> Postazioni { get; set; }
        public List<LicenzaApp> Licenze { get; set; }

        public AppCliente()
        {
            Applicazioni = new List<vLicCliApp>();
            Postazioni = new List<LicCliComp>();
            Licenze = new List<LicenzaApp>();
        }
    }

    public class LicenzaApp
    {
        public string IdKeyApp { get; set; }
        public int Qta { get; set; }
        public DateTime DataScad { get; set; }
        public List<Guid> IdPostazioni { get; set; }


        public LicenzaApp()
        {
            IdPostazioni = new List<Guid>();
        }
    }

    public class Licenza
    {
        public LicClienti Cliente { get; set; }
        public LicKey Key { get; set; }
        public List<LicCliComp> Postazioni { get; set; }
        public List<LicenzaApp> Licenze { get; set; }
        public string Chiave { get; set; }


        public Licenza()
        {
            this.Postazioni = new List<LicCliComp>();
            this.Licenze = new List<LicenzaApp>();
            this.Chiave = "";
        }
    }

}
