﻿using dblu.Licenze.Models;
using dbluGestLic.Classi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using System.IO;
using Telerik.WinControls.Data;
using Newtonsoft.Json;
using NLog;

namespace dbluGestLic
{


    public partial class FrmLicenze : Telerik.WinControls.UI.RadForm
    {
        private static readonly Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private licClient _licclient = new licClient();
        private AppCliente ApplicazioniCliente;
        private List<LicCliComp> ListaPostazioni;
        private const string FILE_LICENZA = "dblu.lic";
        private string PercorsoLic;
        private Licenza LicenzaCorrente;
        private LicKey DatiLicenza;
        public string ChiaveAzienda { get; set; }
        public Guid? ChiavePercorso { get; set; }
        enum Operazione
        {
            Add,
            Remove
        }


        
        public FrmLicenze()
        {
            InitializeComponent();
            this.txtEmailRef.MaskType = Telerik.WinControls.UI.MaskType.EMail;
            this.txtEmailRef.Validating += TxtEmailRef_Validating;
            LicenzaCorrente = null;
        }

        private bool CaricaDatiAzienda()
        {

            if (LicenzaCorrente != null)
            {
                txtPIVA.Enabled = false;
                try
                {
                    txtRagSoc.Text = LicenzaCorrente.Cliente.RagSoc;
                    txtPIVA.Text = LicenzaCorrente.Cliente.IdKeyCli;
                    txtReferente.Text = LicenzaCorrente.Cliente.Referente;
                    txtEmailRef.Text = LicenzaCorrente.Cliente.Email;
                    //txtCognome.Text = Utility.ReadSetting("Cognome");
                    //txtNome.Text = Utility.ReadSetting("Nome");
                    //txtCodiceFiscale.Text = Utility.ReadSetting("CodiceFiscale");
                }
                catch (Exception ex)
                {
                    _logger.Error(ex.Message);
                }

            }
            else
            {
                try
                {
                    Utility.MySettings settings = Utility.MySettings.Load();
                    txtRagSoc.Text = settings.RagioneSociale;
                    txtPIVA.Text = settings.PartitaIva;
                    txtReferente.Text = settings.Referente;
                    txtEmailRef.Text = settings.EmailReferente;
                }
                catch (Exception ex)
                {
                    _logger.Error(ex.Message);
                }


            }
            return VerificaDatiAzienda();
        }

        // Validazione Email
        private void TxtEmailRef_Validating(object sender, CancelEventArgs e)
        {
            bool isValid = ((EMailMaskTextBoxProvider)this.txtEmailRef.MaskedEditBoxElement.Provider).Validate(this.txtEmailRef.Text);
            if (!isValid)
            {
                // TODO:...
            }
        }


        private void RadForm1_Load(object sender, EventArgs e)
        {
            CaricaLicenza();
        }


        private void CaricaLicenza()
        {
            try
            {
                PercorsoLic = dbluBase.dbluFunzioni.GetUNCPath(Path.GetDirectoryName(Application.ExecutablePath));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            // ---------------------------------------------------------------------
            // Per prima cosa verifico se esiste il file dblu.lic
            // ---------------------------------------------------------------------
            if (File.Exists(Path.Combine(PercorsoLic, FILE_LICENZA)))
            {
                try
                {
                    LicenzaCorrente = JsonConvert.DeserializeObject<Licenza>(File.ReadAllText(Path.Combine(PercorsoLic, FILE_LICENZA)));
                    ChiaveAzienda = LicenzaCorrente.Key.IdKeyCli;
                    ChiavePercorso = LicenzaCorrente.Key.IdKey;
                    Utility.MySettings settings = Utility.MySettings.Load();
                    settings.ChiavePercorso = (Guid)ChiavePercorso;
                    settings.Save();
                }
                catch (Exception ex)
                {
                    _logger.Error(ex.Message);
                }
            }
            SetInterfaccia(CaricaDatiAzienda());
        }

        private void CaricaDati()

        {

            // ---------------------------------------------------------------------
            // Mi collego al server dblu delle Licenze
            // ---------------------------------------------------------------------
            if (!_licclient.Login(Application.ProductName, Application.ProductName.Inverti()))
            {
                MessageBox.Show($"Impossibile accedere al server ({_licclient.Message})", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Application.Exit();
            };

            // ---------------------------------------------------------------------
            // Chiamata al server
            // Parametri Input Partita IVA e POSTAZIONE (del Gestiore Licenze)
            // ---------------------------------------------------------------------
            if (ChiaveAzienda == null)
            {
                Utility.MySettings settings = Utility.MySettings.Load();
                ChiaveAzienda = settings.PartitaIva;
            }
            if (ChiavePercorso == null)
            {
                Utility.MySettings settings = Utility.MySettings.Load();
                ChiavePercorso = settings.ChiavePercorso;
                if (String.IsNullOrEmpty(ChiavePercorso.ToString()))
                {
                    ChiavePercorso = (Guid)Guid.NewGuid();
                    settings.ChiavePercorso = (Guid)ChiavePercorso;
                    settings.Save();
                }
            }

            _licclient.GetAppCliente(ChiaveAzienda, ChiavePercorso.ToString(), out ApplicazioniCliente);
            //_licclient.GetAppCliente("00066920935", "F4ACCA9F-5406-4BE4-AA3B-F23626E52E8F", out ApplicazioniCliente);
            //_licclient.GetAppCliente("00066920935", "F54121EB-4CFF-41F5-B95B-56641C5BEA55", out ApplicazioniCliente);

            // ---------------------------------------------------------------------
            // Visualizzo i Moduli di licenza
            // ---------------------------------------------------------------------
            VisualizzaModuli();

            // ---------------------------------------------------------------------
            // Carico l'elenco delle postazione
            // ---------------------------------------------------------------------
            CaricaPostazioni();

            // ---------------------------------------------------------------------
            // Assegno l'elenco delle postazioni e relativi moduli alla griglia
            // ---------------------------------------------------------------------
            VisualizzaPostazioni();
        }

        private bool CaricaPostazioni()
        {
            try
            {
                ListaPostazioni = new List<LicCliComp>();
                foreach (var item in ApplicazioniCliente.Postazioni)
                {
                    LicCliComp l = item;
                    ListaPostazioni.Add(l);
                }

                foreach (string flic in Directory.GetFiles(PercorsoLic, "*.lic"))
                {
                    string fname = Path.GetFileName(flic).ToLower();
                    if (fname != FILE_LICENZA)
                    {
                        try
                        {
                            LicCliComp l = JsonConvert.DeserializeObject<LicCliComp>(File.ReadAllText(flic));
                            LicCliComp c = ApplicazioniCliente.Postazioni.Find(x => x.ComputerName == l.ComputerName | x.CheckPc == l.CheckPc);
                            if (c != null)
                            {
                                l.IdKeyComp = c.IdKeyComp;
                                c.Note = l.Note;
                                //l.Note = c.Note;
                            }
                            else
                            {
                                l.IdKeyComp = Guid.NewGuid();
                                ListaPostazioni.Add(l);
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex.Message);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                return false;
            }

            return true;
        }

        // ---------------------------------------------------------------------
        // VisualizzaPostazioni
        // ---------------------------------------------------------------------
        private void VisualizzaPostazioni()
        {
            DataTable dt1 = new DataTable();
            dt1.Columns.Add("ComputerName", typeof(string));
            dt1.Columns.Add("CheckPc", typeof(string));
            dt1.Columns.Add("IdKeyComp", typeof(System.Guid));
            dt1.Columns.Add("IdKeyApp", typeof(string));
            dt1.Columns.Add("Nome", typeof(string));
            dt1.Columns.Add("Note", typeof(string));
            dt1.Columns.Add("Attiva", typeof(bool));
            foreach (LicCliComp postazioni in ListaPostazioni)
            {
                foreach (vLicCliApp item in ApplicazioniCliente.Applicazioni)
                {
                    bool licenzaAttiva = false;
                    var c = ApplicazioniCliente.Licenze.Find(x => x.IdKeyApp == item.IdKeyApp);
                    if (c != null && postazioni.IdKeyComp != null)
                    {
                        licenzaAttiva = (bool)(c.IdPostazioni.Contains((System.Guid)postazioni.IdKeyComp));
                    }
                    dt1.Rows.Add(postazioni.ComputerName, postazioni.CheckPc, postazioni.IdKeyComp, item.IdKeyApp, item.Nome, postazioni.Note, licenzaAttiva);
                }
            }
            this.rgvPcModuli.DataSource = dt1;

            // Raggruppamento
            GroupDescriptor descriptor = new GroupDescriptor();
            descriptor.GroupNames.Add("ComputerName", ListSortDirection.Ascending);
            descriptor.Expression = "ComputerName ASC,Note ASC";
            descriptor.Format = "PC: {1}";
            this.rgvPcModuli.GroupDescriptors.Clear();
            this.rgvPcModuli.GroupDescriptors.Add(descriptor);

            // 
            this.rgvPcModuli.Columns["ComputerName"].ReadOnly = true;
            this.rgvPcModuli.Columns["CheckPc"].ReadOnly = true;
            this.rgvPcModuli.Columns["CheckPc"].IsVisible = false;
            this.rgvPcModuli.Columns["IdKeyComp"].ReadOnly = true;
            this.rgvPcModuli.Columns["IdKeyComp"].IsVisible = false;
            this.rgvPcModuli.Columns["IdKeyApp"].ReadOnly = true;
            this.rgvPcModuli.Columns["IdKeyApp"].IsVisible = false;
            this.rgvPcModuli.Columns["Nome"].ReadOnly = true;
            this.rgvPcModuli.Columns["Note"].ReadOnly = true;
            this.rgvPcModuli.Columns["Attiva"].ReadOnly = false;

            // Larghezza colonne
            this.rgvPcModuli.MasterTemplate.Columns["Attiva"].Width = 100;
            this.rgvPcModuli.MasterTemplate.Columns["Attiva"].AllowResize = false;
            this.rgvPcModuli.MasterTemplate.AutoExpandGroups = true;
            this.rgvPcModuli.MasterTemplate.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            // Non voglio visuliazzare i gruppi
            //this.rgvPcModuli.MasterTemplate.
        }

        // ---------------------------------------------------------------------
        // VisualizzaModuli()
        // ---------------------------------------------------------------------
        private void VisualizzaModuli()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("IdKeyCli", typeof(string));
            dt.Columns.Add("IdKeyApp", typeof(string));
            dt.Columns.Add("Gruppo", typeof(string));
            dt.Columns.Add("Modulo", typeof(string));
            dt.Columns.Add("Nome", typeof(string));
            dt.Columns.Add("Qta", typeof(int));
            dt.Columns.Add("QtaResidua", typeof(int));
            dt.Columns.Add("DataScad", typeof(DateTime));
            try
            {
                foreach (vLicCliApp item in ApplicazioniCliente.Applicazioni)
                {
                    dt.Rows.Add(item.IdKeyCli, item.IdKeyApp, item.Gruppo, item.Modulo, item.Nome, item.Qta, item.QtaResidua, item.DataScad);
                }
            }
            catch (Exception)
            {
                _logger.Error("Impossibile caricare la lista dei moduli");
            }

            this.radGridViewModuli.DataSource = dt;
            this.radGridViewModuli.ReadOnly = true;
            this.radGridViewModuli.Columns["IdKeyCli"].IsVisible = false;
            this.radGridViewModuli.Columns["IdKeyApp"].IsVisible = false;
            this.radGridViewModuli.Columns["Modulo"].IsVisible = false;
            //this.radGridViewModuli.Columns["Gruppo"].Width = 200;
            //this.radGridViewModuli.Columns["IdKeyApp"].ReadOnly = true;
            ((GridViewDateTimeColumn)this.radGridViewModuli.Columns["DataScad"]).FormatString = "{0:dd/MM/yyyy}";

            GroupDescriptor descriptor = new GroupDescriptor();
            descriptor.GroupNames.Add("Gruppo", ListSortDirection.Ascending);
            this.radGridViewModuli.GroupDescriptors.Clear();
            this.radGridViewModuli.GroupDescriptors.Add(descriptor);

            this.radGridViewModuli.MasterTemplate.AutoExpandGroups = true;
            this.radGridViewModuli.MasterTemplate.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
            //this.radGridViewModuli.Columns[1].BestFit();
        }



        private bool AggiornaQta(string nomeModulo, Operazione operazione)
        {
            bool res = true;
            for (int j = 0; j < radGridViewModuli.Rows.Count; j++)
            {
                if ((string)radGridViewModuli.Rows[j].Cells["IdKeyApp"].Value == nomeModulo)
                {
                    switch (operazione)
                    {
                        case Operazione.Add:
                            radGridViewModuli.Rows[j].Cells["QtaResidua"].Value = (int)radGridViewModuli.Rows[j].Cells["QtaResidua"].Value + 1;
                            break;
                        case Operazione.Remove:
                            if ((int)radGridViewModuli.Rows[j].Cells["QtaResidua"].Value > 0)
                            {
                                radGridViewModuli.Rows[j].Cells["QtaResidua"].Value = (int)radGridViewModuli.Rows[j].Cells["QtaResidua"].Value - 1;
                            }
                            else
                            {
                                res = false;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                }
            }
            return res;
        }

        private void RgvPcModuli_ValueChanged(object sender, EventArgs e)
        {
            // MessageBox.Show("p");
            if (rgvPcModuli.CurrentRow != null)
            {
                rgvPcModuli.EndEdit();
            }
        }

        private void RgvPcModuli_SelectionChanged(object sender, EventArgs e)
        {
            //rgvPcModuli.EndEdit();
            //rgvPcModuli.BeginEdit();
        }

        private void RgvPcModuli_Click(object sender, EventArgs e)
        {
            //rgvPcModuli.EndEdit();
            //rgvPcModuli.BeginEdit();
        }

        private void RgvPcModuli_SelectionChanging(object sender, GridViewSelectionCancelEventArgs e)
        {
            //rgvPcModuli.EndEdit();
            //rgvPcModuli.BeginEdit();
        }

        private void RgvPcModuli_ValueChanging(object sender, ValueChangingEventArgs e)
        {
            //this.rgvPcModuli.Enabled = false;
            bool isChecked = (bool)rgvPcModuli.CurrentRow.Cells["Attiva"].Value;
            if (!(bool)isChecked)
            {
                if (!AggiornaQta((string)rgvPcModuli.Rows[rgvPcModuli.CurrentRow.Index].Cells["IdKeyApp"].Value, Operazione.Remove))
                {
                    MessageBox.Show("Attenzione: Licenze terminate!", "dBlu", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Cancel = true;
                }
            }
            else
            {
                AggiornaQta((string)rgvPcModuli.Rows[rgvPcModuli.CurrentRow.Index].Cells["IdKeyApp"].Value, Operazione.Add);
            }
            //rgvPcModuli.EndEdit();
        }

        private void RadToggleSwitch1_ValueChanged(object sender, EventArgs e)
        {
            this.rgvPcModuli.GroupDescriptors.Clear();
            if (radToggleSwitch1.Value)
            {
                GroupDescriptor descriptor = new GroupDescriptor();
                descriptor.GroupNames.Add("ComputerName", ListSortDirection.Ascending);
                this.rgvPcModuli.GroupDescriptors.Add(descriptor);
            }
            else
            {
                GroupDescriptor descriptor = new GroupDescriptor();
                descriptor.GroupNames.Add("Nome", ListSortDirection.Ascending);
                this.rgvPcModuli.GroupDescriptors.Add(descriptor);
            }
            //this.rgvPcModuli.MasterTemplate.AutoExpandGroups = true;
            //this.rgvPcModuli.MasterTemplate.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
        }

        private void SetInterfaccia(bool attiva)
        {
            if (attiva)
            {
                this.radPageView1.SelectedPage = this.radPageViewPage1;
                this.radPageViewPage1.Enabled = true;
                CaricaDati();
            }
            else
            {
                // DEVO IMPOSTARE I DATI OBBLIGATORI
                this.radPageView1.SelectedPage = this.radPageViewPage2;
                this.radPageViewPage1.Enabled = false;
            }
        }

        private void BtnGenera_Click(object sender, EventArgs e)
        {
            GeneraLicenza();
        }

        // ------------------------------------------------
        //Generazione Licenza
        // ------------------------------------------------
        private void GeneraLicenza()
        {
            Licenza lic = new Licenza
            {
                Cliente = ApplicazioniCliente.Cliente,
                Chiave = ""
            };
            if (LicenzaCorrente != null)
            {
                DatiLicenza = LicenzaCorrente.Key;
                if (DatiLicenza.Path != PercorsoLic)
                    DatiLicenza = null;
            }

            if (DatiLicenza == null)
            {
                DatiLicenza = new LicKey()
                {
                    Path = PercorsoLic,
                    IdKeyCli = ApplicazioniCliente.Cliente.IdKeyCli,
                    IdKey = Guid.NewGuid()
                };
            }
            lic.Key = DatiLicenza;

            Utility.MySettings settings = Utility.MySettings.Load();
            if (settings.Referente != String.Empty)
            {
                lic.Cliente.Referente = settings.Referente;
            }
            if (settings.EmailReferente != String.Empty)
            {
                lic.Cliente.Email = settings.EmailReferente;
            }
            

            // Aggiungo le postazioni
            foreach (LicCliComp pc in ListaPostazioni)
            {
                //LicCliComp pc = ListaPostazioni[0];
                if (pc.IdKeyComp == null)
                {
                    pc.IdKeyComp = Guid.NewGuid();
                }
                pc.IdKeyCli = ApplicazioniCliente.Cliente.IdKeyCli;
                pc.Note = pc.Note;
                lic.Postazioni.Add(pc);
            }

            //Aggiungo le Licenze e le relatvie postazioni
            foreach (var licenza in ApplicazioniCliente.Applicazioni)
            {
                LicenzaApp la = new LicenzaApp()
                {
                    IdKeyApp = licenza.IdKeyApp,
                    DataScad = DateTime.Today.AddMonths(3)
                };
                int qta = 0;
                for (int r = 0; r < this.rgvPcModuli.RowCount; r++)
                {
                    if (rgvPcModuli.Rows[r].Cells["IdKeyApp"].Value.ToString().Equals(licenza.IdKeyApp))
                    {
                        if ((bool)rgvPcModuli.Rows[r].Cells["Attiva"].Value)
                        {
                            qta += 1;
                            System.Guid tmp = new System.Guid(rgvPcModuli.Rows[r].Cells["IdKeyComp"].Value.ToString());
                            la.IdPostazioni.Add((Guid)tmp);
                        }
                    }
                }
                la.Qta = qta;
                lic.Licenze.Add(la);
            }

            //Invio la richiesta
            // per il debug.
            //File.WriteAllText("c:\\temp\\richiesta.json", JsonConvert.SerializeObject(lic));
            if (_licclient.RegistraAppCliente(lic, out Licenza licfirmata))
            {
                String pathDest = Path.Combine(PercorsoLic, FILE_LICENZA);
                File.WriteAllText(pathDest, JsonConvert.SerializeObject(licfirmata));
                MessageBox.Show("Chiave Generata", "dBlu", MessageBoxButtons.OK, MessageBoxIcon.Information);
            } 
            else
            {
                MessageBox.Show("Chiave NON Generata", "dBlu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void BtnSalva_Click(object sender, EventArgs e)
        {
            SalvaImpostazioni();
        }

        private void SalvaImpostazioni()
        {
            if (!ValidaDati())
            {
                MessageBox.Show("Dati non corretti", "dblu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                Utility.MySettings settings = Utility.MySettings.Load();
                settings.RagioneSociale = txtRagSoc.Text;
                settings.PartitaIva = txtPIVA.Text;
                settings.Referente = txtReferente.Text;
                settings.EmailReferente = txtEmailRef.Text;
                settings.CodiceFiscale = txtCodiceFiscale.Text;
                settings.Cognome = txtCognome.Text;
                settings.Nome = txtNome.Text;
                settings.Save();

                MessageBox.Show("Dati Salvati", "dblu", MessageBoxButtons.OK, MessageBoxIcon.Information);
                SetInterfaccia(VerificaDatiAzienda());
            }
            catch (Exception)
            {
                MessageBox.Show("Dati non Salvati", "dblu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool ValidaDati()
        {
            bool res;
            res = (txtRagSoc.Text != "" & txtPIVA.Text != "");
            if (!res)
            {
                res = (txtCognome.Text != "" & txtNome.Text != "" & txtCodiceFiscale.Text != "");
            }
            return res;
        }

        private bool VerificaDatiAzienda()
        {
            return (txtRagSoc.Text != "" & txtPIVA.Text != "");
        }

        private void RadButton1_Click(object sender, EventArgs e)
        {
            FrmCambiaPassword fPassword = new FrmCambiaPassword();
            if (fPassword.ShowDialog() == DialogResult.OK)
            {

            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            CaricaLicenza();
        }


        
    }
}
