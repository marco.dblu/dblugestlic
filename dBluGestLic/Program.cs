﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using Microsoft.VisualBasic.ApplicationServices;   // Add reference to Microsoft.VisualBasic


namespace dbluGestLic
{
    class Program : WindowsFormsApplicationBase
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            try
            {

            }
            catch (Exception)
            {

                throw;
            }


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            string procName = Process.GetCurrentProcess().ProcessName;
            Process[] processes = Process.GetProcessesByName(procName);
            if (processes.Length > 1)
            {
                MessageBox.Show(procName + " already running");
                return;
            }
            else
            {
                frmPassword fPassword = new frmPassword();
                if (fPassword.ShowDialog() == DialogResult.OK)
                {
                    using (FrmLicenze fmain = new FrmLicenze())
                    {
                        fmain.MaximumSize = Screen.FromRectangle(fmain.Bounds).WorkingArea.Size;
                        fmain.Left = (Screen.PrimaryScreen.Bounds.Width - fmain.Width) /2;
                        fmain.Top = (Screen.PrimaryScreen.Bounds.Height - fmain.Height) / 2;
                        //f.Icon = My.Resources.d_blu;
                        fmain.ShowDialog();
                    }
                }
            }
        }
    }
}