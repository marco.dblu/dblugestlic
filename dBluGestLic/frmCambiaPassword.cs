﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using dbluGestLic.Classi;

namespace dbluGestLic
{
    public partial class FrmCambiaPassword : Telerik.WinControls.UI.RadForm
    {
        public FrmCambiaPassword()
        {
            InitializeComponent();
        }

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            if (Verifica())
            {
                if (SalvaDati())
                {
                    MessageBox.Show("Password impostata!", "dblu", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.OK;
                }
                else
                {
                    MessageBox.Show("Si è verifato un errore", "dblu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Le password inserite non sono corrette oppure non coincidono", "dblu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private bool Verifica()
        {
            return (txtPassword.Text != String.Empty & txtConfermaPassword.Text != String.Empty & txtPassword.Text == txtConfermaPassword.Text);
        }

        private bool SalvaDati()
        {
            string hash;  // hash della password salvata
            try
            {
                {
                    using (MD5 md5Hash = MD5.Create())
                    {
                        hash = Utility.GetMd5Hash(md5Hash, txtPassword.Text);
                    }
                    Utility.MySettings settings = Utility.MySettings.Load();
                    settings.Password = hash;
                    settings.Save();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        private void TxtConfermaPassword_Validating(object sender, CancelEventArgs e)
        {
            if (txtConfermaPassword.Text == string.Empty | txtConfermaPassword.Text != txtPassword.Text)
            {
                MessageBox.Show("Le due password non coincidono");
                e.Cancel = true;
            }
        }

        private void TxtPassword_Validating(object sender, CancelEventArgs e)
        {
            if (txtPassword.Text == string.Empty)
            {
                MessageBox.Show("La password non può essere vuota");
                e.Cancel = true;
            }
        }

        private void RadButton1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}