﻿Imports System.IO
Imports Newtonsoft.Json
Imports dbluBase
Imports System.Runtime.InteropServices
Imports System.Reflection
Imports System.ComponentModel

Public Class frmpath

    Private xPostazione As Postazione = New Postazione()
    Private xPercorso As PercorsoLic = New PercorsoLic()
    Private Const FileLic = "dblu.lic"
    Private Utente As String

    Private Sub BtnFolder_Click(sender As Object, e As EventArgs) Handles btnFolder.Click

        fbdpathLic.SelectedPath = txtPath.Text
        If fbdpathLic.ShowDialog() = DialogResult.OK Then
            txtPath.Text = GetUNCPath(fbdpathLic.SelectedPath)
        End If

    End Sub



    Private Sub Frmpath_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utente = Environment.UserName
        If Utente <> "" Then
            Me.Text = String.Concat(Me.Text, " (", Utente, ")")
        End If
        Dim flic = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), FileLic)
        Try
            Dim lp As New dbluBase.LicPostazione()

            If File.Exists(flic) Then
                xPercorso = JsonConvert.DeserializeObject(Of PercorsoLic)(File.ReadAllText(flic))
                txtPath.Text = xPercorso.Percorso
            End If
            If lp.Init() Then
                xPostazione.CheckPc = lp.Check
                xPostazione.ComputerName = lp.ComputerName
                xPostazione.Note = "PC di " & Utente
                xPercorso.ComputerName = lp.ComputerName
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BtnSalva_Click(sender As Object, e As EventArgs) Handles btnSalva.Click
        Try
            txtPath.Text = GetUNCPath(txtPath.Text)
            If Directory.Exists(txtPath.Text) Then

                Dim filePostazione As String = Path.Combine(txtPath.Text, $"{xPostazione.ComputerName}.lic")
                If File.Exists(filePostazione) Then
                    File.Delete(filePostazione)
                End If
                File.WriteAllText(filePostazione, JsonConvert.SerializeObject(xPostazione))
                Dim flic = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), FileLic)
                If File.Exists(flic) Then
                    File.Delete(flic)
                End If
                xPercorso.Percorso = txtPath.Text
                File.WriteAllText(flic, JsonConvert.SerializeObject(xPercorso))
            End If
        Catch ex As Exception

        End Try
    End Sub


    Protected Function GetUNCPath(strpath As String) As String
        Dim strDrive As String = strpath.Trim()
        Dim buffer As IntPtr = IntPtr.Zero
        Dim size As Integer = 0
        Dim apiRetVal As Integer
        Try
            apiRetVal = WNetGetUniversalName(strDrive, &H1, CType(IntPtr.Size, IntPtr), size)
            If apiRetVal <> 234 Then
            Else
                buffer = Marshal.AllocCoTaskMem(size)
                apiRetVal = WNetGetUniversalName(strDrive, &H1, buffer, size)

                If apiRetVal = 0 Then
                    strDrive = Marshal.PtrToStringAuto(New IntPtr(buffer.ToInt64() + IntPtr.Size), size)
                    strDrive = strDrive.Substring(0, strDrive.IndexOf(vbNullChar))
                End If
            End If
            If apiRetVal <> 0 AndAlso strDrive.Contains(":") Then
                Dim disco = strDrive.Split(":")
                strDrive = $"\\{Environment.MachineName}\{disco(0)}${disco(1)}"
            End If
        Finally
            Marshal.FreeCoTaskMem(buffer)
        End Try
        Return strDrive
    End Function

    <DllImport("mpr.dll", CharSet:=CharSet.Unicode)>
    Private Shared Function WNetGetUniversalName(ByVal lpLocalPath As String, <MarshalAs(UnmanagedType.U4)>
    ByVal dwInfoLevel As Integer, ByVal lpBuffer As IntPtr, <MarshalAs(UnmanagedType.U4)>
    ByRef lpBufferSize As Integer) As Integer
    End Function

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Auto), Serializable()>
    Class MyUNC
        <MarshalAs(UnmanagedType.LPStr)>
        Public UniversalName As String
        <MarshalAs(UnmanagedType.LPStr)>
        Public ConnectionName As String
        <MarshalAs(UnmanagedType.LPStr)>
        Public RemainingPath As String
    End Class

End Class


Public Class PercorsoLic
    Private _computerName As String = ""
    Private _percorso As String


    Public Property ComputerName As String
        Get
            Return _computerName
        End Get
        Set(value As String)
            _computerName = value
        End Set
    End Property

    Public Property Percorso As String
        Get
            Return _percorso
        End Get
        Set(value As String)
            _percorso = value
        End Set
    End Property

End Class


Public Class Postazione
    Public Property ComputerName As String = ""
    Public Property IdKeyComp As String
    Public Property CheckPc As String
    Public Property Note As String = ""
End Class